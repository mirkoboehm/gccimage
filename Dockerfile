# Generate mininmal Ubuntu image with custom gcc 13
FROM ubuntu:22.04
WORKDIR /build
ENV DEBIAN_FRONTEND=noninteractive
ENV DEBCONF_NONINTERACTIVE_SEEN true
ENV TZ=Etc/UTC
RUN apt-get -qq update 
RUN apt-get -qq install -y git gcc g++ gperf bison flex texinfo help2man make libncurses5-dev \
    python3-dev autoconf automake libtool libtool-bin gawk wget bzip2 xz-utils unzip \
    patch libstdc++6 rsync git meson ninja-build
RUN git clone https://github.com/crosstool-ng/crosstool-ng
WORKDIR /build/crosstool-ng
# RUN git checkout crosstool-ng-1.25.0
RUN git checkout b42079e92a0710bc56ace3267cad5132fc28d520
RUN ./bootstrap
RUN ./configure --prefix=/crosstool-ng
RUN make
RUN make install
RUN cp /crosstool-ng/share/bash-completion/completions/ct-ng /etc/bash_completion.d/
ENV PATH="${PATH}:/crosstool-ng/bin"
WORKDIR /build
# later:
# RUN rm -rf crosstool*
RUN mkdir x86_64-multilib-linux-gnu
WORKDIR /build/x86_64-multilib-linux-gnu
# this create a new template configuration, we supply our own:
# RUN ct-ng x86_64-multilib-linux-gnu
ADD x86_64-multilib-linux-gnu.config .config
RUN ct-ng source
ARG jobs=1
RUN ct-ng build.$jobs
